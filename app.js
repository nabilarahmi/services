const express = require('express');
var bodyParser = require('body-parser');
const project = require('./project');
const postgre = require('./postgre');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/*
//get response di param
app.get('/', function(req,res,next) {
    postgre.query("SELECT * FROM PO_item", function(err, result) {
        if (err) {
            res.status(500).json({
                message: err.message
            })
        } else {
            res.status(200).json({
                message : 'hai',
                name : req.params.nama
            });
        }
    })
});
//post response di body
app.post('/', function(req,res,next) {
    res.status(200).json({
        message : 'hai POST',
        name : req.body.nama
    });
});
*/

//ORDER
//get pesanan
app.get('/order', function(req,res,next){
    postgre.query("SELECT * FROM Purchase_Order ORDER BY order_id", function(err,result){
        if (err){
            res.status(500).json({
                message: err.message
            });
        }
        if (!result[0]) {
            res.status(404).json({
                message: 'Record not found'
            });
        }else{
            res.status(200).json(result.rows)
        }
    })
})

//get pesanan dari customer_id tertentu
app.get('/order_customer', function(req,res,next){
    postgre.query('SELECT * FROM Purchase_Order WHERE customer_id= $1',[customer_id], function(err,result){
        if (err) throw err;
        console.log(result);
        if (!result[0]) {
            return res.status(404).json({
                message: 'Record not found'
            });
        }
        return res.send(result[0]);
    })
})
//Agen
//Menampilkan seluruh agen
app.get('/agen', function(req,res,next){
    postgre.query("SELECT * FROM Agen ORDER BY agent_id", function(err,result){
        if (err){
            res.status(500).json({
                message: err.message
            })
        }else{
            res.status(200).json(result.rows)
        }
    })
})

//User-recommendation
//Menampilkan banyaknya jumlah pesanan oleh customer_id tertentu
app.get('/user_recommend',function(req,res,next){
    postgre.query("SELECT COUNT (DISTINCT customer_id) from Transaction_History", function(req,res,next){
        if (err) throw err;
        console.log(result);
        if (!result[0]) {
            return res.status(404).json({
                message: 'Record not found'
            });
        }
        return res.send(result[0]);
    })
})
//Menampilkan tenants paling banyak
app.get('/tenants_recommend', function(req,res,next){
    postgre.query("SELECT COUNT (DISTINCT merchant_id) from Transaction_History", function(req,res,next){
        if (err) throw err;
        console.log(result);
        if (!result[0]) {
            return res.status(404).json({
                message: 'Record not found'
            });
        }
        return res.send(result[0]);
    })
})
//Menampilkan produk paling banyak
pp.get('/product_recommend', function(req,res,next){
    postgre.query("SELECT COUNT (DISTINCT item_id) from PO_Item", function(req,res,next){
        if (err) throw err;
        console.log(result);
        if (!result[0]) {
            return res.status(404).json({
                message: 'Record not found'
            });
        }
        return res.send(result[0]);
    
// Pembayaran dan RFID Postpone
// tidak ada penyelesaian dari kelompok sebelumnya

//Pengiriman
//Mencari berdasarkan order_id

//Mercari berdasarkan shipping_id
//Laporan

// app.use('/project', project);

app.use(function(req, res, next) {
    res.status(404).json({
        message: "NOT FOUND WOY"
    });
})

app.listen(3001, function() {
    
})

module.exports = app;